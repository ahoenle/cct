.PHONY: test upload clean bootstrap

init: _virtualenv
ifneq ($(wildcard requirements.txt),)
	venv/bin/pip install -r requirements.txt
endif
	make clean

test:
	sh -c '. venv/bin/activate; venv/bin/python -m py.test tests/'


_virtualenv:
	virtualenv venv
	venv/bin/pip install --upgrade pip
	venv/bin/pip install --upgrade setuptools
