krakenex==0.1.4
pandas==0.20.3
matplotlib==2.0.2
seaborn==0.7.1
records==0.5.2
progressbar2==3.34.2
