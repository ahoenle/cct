``cct`` – A **c**rypto **c**urrency **t**rader

# File content

| File Name | Brief |
|---|---|
`CCTExceptions.py` | Custom exceptions |
`Kraken.py`        | More high level `krakenex` wrapper |
`TickerStore.py`   | Store Kraken Ticker |
`auxmath.py`       | Auxiliary math functions |
`auxtools.py`      | Auxiliary classes, functions, and variables |
`cct.py`           | Crypto Trader engine |
`httpsServer.py`   | Fake Kraken server |
`liveTicker.py`    | Latest `TickerStore` events |
`plotRate.py`      | obsolete? |
`plotTicker.py`    | obsolete? |
`sqliteToMySQL.py` | *WIP:* Planned to migrate to MySQL |
`syncDB.py`        | *WIP:* Planned to migrate to MySQL |


# Development notes

* this is a `python3` project
    * tested with python 3.6.1
* best run in virtual environment

~~~bash
python -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
~~~

## Status (Last Update 16 Oct 2017)

* Running `TickerStore`
    * Saving to sqlite db 
* Running `liveTicker`
* Developing `ema_trend` strategy

## Known issues (Last Update 16 Oct 2017)

* **Acquired options should be stored in database, not memory**

* The live ticker is sometimes rather slow.
    * Likely the following line of code

      ~~~python
      # Converting dates column to float values
      candleframe['Date'] = candleframe['Date'].map(mdates.date2num)
      ~~~ 

    * In principle, the dates are available as number and don't need to be converted. But I couldn't implement it nicely.

## Important TODOs (Last Update 16 Oct 2017)

* Show candle info and ema/sma values on mouse hover
* Implement buy and close methods in APIs
  * Start with fake API
* Make liveTicker read from fake datebases
  * Make TickerStore write to fake databases - based on fake clock?

## The fakeKrakenAPI class

It's there to emulate the responses that the Kraken API would send.

Inheriting from `http.server.BaseHTTPRequestHandler` it can only deal with simple `GET` and `POST` commands, but that's enough.

A `POST` command is decoded in the `do_POST()` method.

The handler first sends a positive response and decodes the arguments of the `POST` call.

~~~python
self.send_response(207)
self.end_headers()
args = self.convert_args(post_body)
~~~

The Kraken API has a couple of methods, like e.g. Ticker, AssetPairs, etc. One can call them by appending the method name to the URL. For example, to call the Ticker one would send the request to `https://api.kraken.com/0/public/Ticker`. The `fakeKrakenAPI` has the attribute `path` in which the called URL is stored. So we investigate the path and call the corresponding method.

~~~python
if 'Time' in self.path:
    query_result = self.Time()
elif 'Assets' in self.path:
    if self.check_args(args, opt_args=['aclass', 'asset']):
        query_result = self.Assets()
elif 'Ticker' in self.path:
    if self.check_args(args, required_args=['pair']):
        query_result = self.Ticker()
elif 'Setup' in self.path:
    if self.check_args(args, required_args=[]):
        query_result = self.Setup()
else:
    self.error.append(APIE['unk_method'])
~~~

Sometimes before calling a function the arguments are checked.
There are required arguments (`required_args`) and optional arguments (`opt_args`). See https://www.kraken.com/help/api for details.

`Setup()` is a special function that is not there for the real API. It sets up *clock* and *data*.  Having a modified clocks allows you to load data that has information from the past. Currently data can only be a simple csv file with a fixed format. This needs to be extended to csv with free formattings, json, SQL, and/or others.
The attributes `clock` and `data` are stored in the `HTTPServer` instance by invoking

~~~python
# in fakeKrakenAPI.Setup():
self.server.data = pd.read_csv(datapath)
self.server.clock = Clock(t0=self.server.data.time[0], multiplier=100)
~~~

Note the *server* in `self.server.data = ...`.

The data and clock cannot be attributes of `fakeKrakenAPI` because they would be lost after the current request is finished. The underlying problem is that `HTTPServer` gets a request handler *class* and not *an instance* of said class when it's constructed. Any request handler attributes are gone after the request is finished. Fortunately each request handler has access to the server instance with `self.server`. Currently the server lives forever.

## The TickerStore class

`TickerStore` is there to periodically check the Kraken Ticker and store the result in a csv file.


## auxtools

I wrote some auxiliary tools that help with the data handling.

### helpful variables

... as defined in auxtools

~~~python
# all available asset pairs
ALL_ASSET_PAIRS
# names of columns in database -- the information from the Ticker command
names
~~~


### helper classes

~~~python
# data from a single candlestick bin
class candlestick(object)

# Ticker() message
class TickerInfo(object)
~~~


### helper functions

~~~python
# insert the unixtime as first column in csv data
def csvinsertmtime()
# invoke via 
python -c 'from auxtools import csvinsertmtime; csvinsertmtime()'

# convert csv to mysql database
def csvtomysql()
# invoke via
python -c 'from auxtools import csvtosqlite; csvtosqlite()'

# merge sqlite databases
def sqlitemerge(db_in = None, db_out = None)
# invoke via
python -c 'from auxtools import sqlitemerge; sqlitemerge(<db_in>, <db_out>)'

# split sqlite databases day-by-day
def sqlitesplit(db_in)
# invoke via
python -c 'from auxtools import sqlitesplit; sqlitesplit(<db_in>)'
~~~

The helper functions have hardcoded paths. So make sure to check the source code before executing them.


### interesting stuff

Playlist with interesting videos:

* https://www.youtube.com/playlist?list=PLWeS_2BTH6_Djap9Jx4Nwv68SoW-3gFpY
