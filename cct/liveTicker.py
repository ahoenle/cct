import datetime as dt
import time
import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.dates import DateFormatter
from matplotlib.finance import candlestick_ohlc
import matplotlib.dates as mdates
import pandas as pd
import records
import sqlalchemy
import numpy as np
import os
import pytz

from auxmath import SimpleMovingAverage
from auxmath import ExpMovingAverage
from auxtools import candlestick

from liveTickerBase import LiveTickerBase

from CCTExceptions import DatabaseNotAvailable

import logging
# create logger
logger = logging.getLogger('cct.liveTicker')
module_logger = logging.getLogger('liveTicker')
module_logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('cct.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the module_logger
module_logger.addHandler(fh)
module_logger.addHandler(ch)


try:
    os.environ['CCT']
except KeyError:
    logger.critical('Error "CCT" environment variable (local cct path) not set.')
    exit()

# Date formatter for axis
xfmt = DateFormatter('%d.%m. - %H:%M:%S %Z')


class LiveTicker(LiveTickerBase):
    """Work with real data"""
    def __init__(self, group_frequency=3, latest_n=4500,
                 assetpair='XXRPZEUR', cryptocur='XRP', cur='EUR',
                 update_freq=5):
        super(LiveTicker, self).__init__(group_frequency, latest_n,
                                             assetpair, cryptocur, cur, update_freq)
        self.sleep_time = group_frequency * 60. / 20.
        self.setup_db()

    def setup_db(self):
        """Setup the records db connection to today's db
        """
        t = time.gmtime()
        db_file = self.dbfile_from_time(t)
        try:
            self.db = records.Database('sqlite:///{}'.format(db_file))
        except sqlalchemy.exc.OperationalError:
            raise DatabaseNotAvailable(
                'Are you sure the database exists under: {}?'
                .format(db_file))
        self.db_selected_day = t.tm_mday

    def dbfile_from_time(self, struct_time):
        db_out_dir = os.path.join(
            os.environ['CCT'], 'databases', 'TickerStore',
            str(struct_time.tm_year), str(struct_time.tm_mon))
        db_dir = os.path.join(
            os.environ['CCT'], 'databases', 'TickerStore',
            str(struct_time.tm_year), str(struct_time.tm_mon))
        db_file = os.path.join(db_out_dir,
                               '{}.sqlite3'.format(struct_time.tm_mday))
        return db_file

    def grab_from_db(self):
        """Grab data from database and return as dataframe"""
        # check if correct db is selected
        if time.gmtime().tm_mday != self.db_selected_day:
            self.setup_db()
        # grab latest entries from database
        try:
            rows = self.db.query('SELECT * FROM'
                                 ' (SELECT * FROM {} ORDER BY time DESC LIMIT {})'
                                 'sub ORDER BY unixtime'
                                 .format(self.assetpair, self.latest_n))
        except:
            # TODO: Error handling
            raise

        """
              The consistency check is very time consuming
        TODO: Maybe check only new entires?
              ... I never encountered an error with this
              ... DB seems always to be in a good shape
        """
        # for i, row in enumerate(rows):
        #     thistime = float(row.unixtime)
        #     if thistime < lasttime:
        #         raise ConsistencyError('Time should be strictly increasing '
        #                                '(error in line {}'.format(i))
        #     lasttime = thistime
        # n_selected = i + 1

        dataframe = rows.export('df')
        n_selected = len(dataframe)
        if n_selected < self.latest_n:
            to_read = self.latest_n - n_selected
            logger.info('Need to grab {} entries from buffer'.format(to_read))
            if hasattr(self, 'df_buffer'):
                logger.debug('Reading from self.df_buffer')
                start_idx = -1 * to_read
                dataframe = self.df_buffer.ix[start_idx:].copy().append(dataframe)
                logger.debug('Done reading from self.df_buffer')
                logger.debug('Length of dataframe is now {}'.format(len(dataframe)))
            else:
                from datetime import date, timedelta
                logger.info('self.df_buffer not found - create & recurse')
                yesterday = date.today() - timedelta(1)
                t = yesterday.timetuple()
                db_file = self.dbfile_from_time(t)
                logger.debug('Yesterday\'s db_file: {}'.format(db_file))
                prev_db = records.Database('sqlite:///{}'.format(db_file))
                prev_rows = prev_db.query(
                    'SELECT * FROM'
                    ' (SELECT * FROM {} ORDER BY time DESC LIMIT {})'
                    'sub ORDER BY unixtime'
                    .format(self.assetpair, self.latest_n - n_selected))
                self.df_buffer = prev_rows.export('df')
                logger.debug('self.df_buffer initialized with length {}'
                            .format(len(self.df_buffer)))
                dataframe = self.grab_from_db()
        else:
            # start buffering only if it's late and we need to grab a new db soon
            t = time.gmtime()
            if t.tm_hour == 23 and t.tm_min > 55:
                logger.info('Day is ending; buffering dataframe.')
                self.df_buffer = dataframe.copy()
        logger.debug('grabbed from db. Returning dataframe with length {}'
                    .format(len(dataframe)))
        return dataframe





if __name__ == '__main__':
    logger = module_logger
    ticker = LiveTicker()
    ticker.show_animated()
