import http.server
import ssl
import os
import json
import pandas as pd
import time

from clock import Clock
from datetime import datetime
from wsgiref.handlers import format_date_time
from time import mktime


"""
all that you need is on these pages

Kraken API:
-----------

https://www.kraken.com/help/api



Request Handler faking the API:
-------------------------------

https://stackoverflow.com/questions/19705785/python-3-https-webserver
https://daanlenaerts.com/blog/2015/06/03/create-a-simple-http-server-with-python-3/

"""

# API Errors
APIE = {
    'unk_method': 'EGeneral:Unknown method',
    'inv_arg': 'EGeneral:Invalid arguments',
    'missing_arg': 'EGeneral:Missing argument',
    'not_implemented': 'EInternal:Not implemented?',
    'inv_conf': 'EInternal:Server not configured',
    'entry_not_found': 'EInternal:No entry for given time'
    }


# HTTPRequestHandler class
class fakeKrakenAPI(http.server.BaseHTTPRequestHandler):
    """Emulate the Kraken API

    This is the goal of the API:
    Supports the commands as specified by the Kraken API and returns replies
    based on arbitrary data.
    Inherits from http.server.BaseHTTPRequestHandler

    Attributes:
        error: Array of errors of current request
        data: Trading data (TODO: Support CSV & JSON)
        clock: Clock to synchronize trading, uses time range as defined in data

    """

    def __init__(self, args, *kwargs):
        """Initialize.

        Call base class init and initialize attributes that are used for
        the handling of the current request.

        Args:
            args: passed on to base class __init__()
            kwargs: passed on to base class __init__()

        Returns:
            None

        Raises:
            Only when base class raises.
        """
        super().__init__(args, *kwargs)
        self.error = []
        self.data = None


    # GET
    def do_GET(self):
        # Send response status code
        self.send_response(201)

        # Send headers
        self.send_header('Content-type','text/html')
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

        # Send message back to client
        message = "{\"error\":[],\"result\":{\"unixtime\":1504905000,\"rfc1123\":\"Fri,  8 Sep 17 21:10:00 +0000\"}}"
        # message = '<pre style="word-wrap: break-word; white-space: pre-wrap;">{"error":[],"result":{"unixtime":1504905000,"rfc1123":"Fri,  8 Sep 17 21:10:00 +0000"}}</pre>'
        # Write content as utf-8 data
        self.wfile.write(bytes(message, "utf8"))
        return

    def do_POST(self):
        """Handle POST commands.

        Emulate behaviour of Kraken API for POST commands. Vastly uses class
        attributes to share data between that are called from within this method.

        Args:
            None

        Returns:
            None

        Raises:
            None
        """
        self.send_response(207)
        self.end_headers()

        self.error = []
        # print('Headers:         ' + str(self.headers))
        # print('Command:         ' + str(self.command))
        # print('Path:            ' + str(self.path))
        # print('Request-version: ' + str(self.request_version))
        content_len = int(self.headers['content-length'])
        post_body = self.rfile.read(content_len).decode('utf-8')
        print('Body:            ' + post_body)
        args = self.convert_args(post_body)
        query_result = {}
        if 'Time' in self.path:
            query_result = self.Time()
        elif 'Assets' in self.path:
            if self.check_args(args, opt_args=['aclass', 'asset']):
                query_result = self.Assets()
        elif 'Ticker' in self.path:
            if self.check_args(args, required_args=['pair']):
                query_result = self.Ticker()
        elif 'Setup' in self.path:
            if self.check_args(args, required_args=[]):
                query_result = self.Setup()
        else:
            self.error.append(APIE['unk_method'])

        print('query_result:    ' + str(query_result))

        message = { 'error': self.error, 'result': query_result }
        message = json.dumps(message)
        self.wfile.write(bytes(message, 'utf8'))

    def convert_args(self, post_body):
        """Convert post body into list of arguments.

        Args:
            post_body: POST body as string

        Returns:
            Dictionary with arguments.
            keys: argument
            values: argument value

        Raises:
            # TODO: Raise when errors in string handling occur
        """
        if not post_body:
            return {}
        key_val_strings = [post_body]
        if '&' in post_body:
            key_val_strings = post_body.split('&')
        try:
            d = { a.split('=')[0]: a.split('=')[1]
                for a in key_val_strings }
        # just raise for now
        except:
            raise
        return d

    def Time(self):
        """Handle Time request.

        Args:
            None

        Returns:
            Dictionary with two entries.
            'unixtime': time since the epoch in seconds
            'rfc1123': time formatted in rfc1123 standard

        Raises:
            None
        """
        t = datetime.now()
        stamp = mktime(t.timetuple())
        return { 'unixtime': t.strftime('%s'),
                 'rfc1123': format_date_time(stamp) }

    def Assets(self, info='all', aclass='currency', asset='XXRP'):
        """Handle Assets request.

        Args:
            info: info to retrieve (default: all)
            aclass: asset class (default: currency)
            asset: comma delimited list of assets to get info on (default: XXRP)

        Returns:
            Result dictionary with arrays [asset name, info1, info2, ...]

        Raises:
            None
        """
        return  { 'message': 'Assets received successfully but '
                  'fake API has no further information to return',
                  'info': info,
                  'aclass': aclass,
                  'asset': asset }

    def Ticker(self, pair='XXRPZEUR'):
        """Handle Ticker request.

        Args:
            pair: comma delimited list of asset pairs to get info on
                  (default: XXRPZEUR)

        Returns:
            Result dictionary with arrays [pair name, info1, info2, ...]

        Raises:
            None
        """
        if not hasattr(self.server, 'clock'):
            self.error.append(APIE['inv_conf'])
            return {}
            # TODO
            # Dirty fix for testing. Setup() creates the clock and stores it
            # in self.clock. But the Server does not create an instance of the
            # fake API and hence the clock is lost at the next call
            # rq_handler = fakeKrakenAPI()
            # rq_handler.Setup()
            # vs.
            # fakeKrakenAPI.Setup()
            # self.Setup()
        t = self.server.clock.time()
        for tentry, close in zip(self.server.data.time, self.server.data.close):
            tentry = time.mktime(time.strptime(tentry, '%Y-%m-%d'))
            if t - tentry > 0:
                break
            else:
                continue
            tentry = 0
            close = 0
            # TODO: Only for now. Fails if last entry is the correct one.
            self.error.append(APIE('entry_not_found'))
        # find closest entry
        # TODO: make more efficient
        return { 'message': '{} @ {}: {} (close)'
                 .format(pair,
                         time.strftime('%a, %d %b %Y %H:%M:%S +0000',
                                       time.localtime(t)),
                         close),
                 'pair': pair  }

    def Setup(self, datapath='data/'
              'hour_CryptoCompare_Index_XRP_EUR_117_121505145613382.csv'):
        """Setup the server with fake data.

        ###
        ### TODO
        ###

        """
        self.server.data = pd.read_csv(datapath)
        self.server.clock = Clock(t0=self.server.data.time[0], multiplier=100)
        return { 'message': 'Setup received successfully' }

    def check_args(self, args, required_args=[], opt_args=[]):
        """Check arguments.

        Args:
            args: Input arguments from POST request
            required_args: Mandatory arguments
            opt_args: Optional arguments

        Returns:
            True: if all arguments fullfil requirements
            False: else

        Raises:
            Exception: Always if try fails # TODO: Error handling
        """
        for a in required_args:
            if a not in args.keys():
                self.error.append(APIE['missing_arg'])
                return False
        try:
            for a in args.keys():
                if not (a in required_args or a in opt_args):
                    self.error.append(APIE['inv_arg'])
                    return False
        # don't know yet which types of exceptions might occur, just raise
        except:
            raise
        else:
            return True


server_address = ('localhost', 4443)
certfile = os.path.join('data', 'keys', 'localhost.pem')
certkey = os.path.join('data', 'keys', 'localkey.pem')
httpd = http.server.HTTPServer(server_address, fakeKrakenAPI)
httpd.socket = ssl.wrap_socket(httpd.socket,
                               server_side=True,
                               certfile=certfile,
                               keyfile=certkey,
                               ssl_version=ssl.PROTOCOL_TLSv1)
httpd.serve_forever()
