import datetime as dt
import time
import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.dates import DateFormatter
from matplotlib.finance import candlestick_ohlc
import matplotlib.dates as mdates
import pandas as pd
import records
import seaborn as sns
import numpy as np
import os
import pytz

from auxmath import SimpleMovingAverage
from auxmath import ExpMovingAverage
from auxtools import candlestick

import logging
# create logger
logger = logging.getLogger('cct.liveTickerBase')
module_logger = logging.getLogger('liveTickerBase')
module_logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('cct.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the module_logger
module_logger.addHandler(fh)
module_logger.addHandler(ch)


try:
    os.environ['CCT']
except KeyError:
    logger.critical('Error "CCT" environment variable (local cct path) not set.')
    exit()

# Date formatter for axis
xfmt = DateFormatter('%d.%m. - %H:%M:%S %Z')

# fig = plt.figure()

sns.set_style('whitegrid', {'grid-color': '1.0'})


class LiveTickerBase(object):
    """
       WRITEME
    """

    def __init__(self, group_frequency=3, latest_n=4500,
                 assetpair='XXRPZEUR', cryptocur='XRP', cur='EUR',
                 update_freq=1):
        """
        Args:
            group_frequency: bin size of candlesticks in candlestick plot
                             (in minutes, default: 3)
            latest_n:        number of data points for candlestick plots
                             (NOT number of candlesticks, default: 4500)
            assetpair:       AssetPair (as defined in TickerStore database
            crtypocur:       Crypto Currency (only for axis labels)
            cur:             Real Currency (only for axis labels)
            update_freq:     Update frequency (seconds)
            time_latest_bin: Float time of latest bin
            db:              database connection
            db_selected_day: day of currently selected database
            df_buffer:       buffered db entries for day boundaries
            dataframe:       Dataframe of latest query
            time_series:     Time Series
        """
        self.group_frequency = group_frequency
        self.latest_n = latest_n
        self.assetpair = assetpair
        self.cryptocur = cryptocur
        self.cur = cur
        self.update_freq = update_freq
        self.time_latest_bin = 0
        self.fig, self.ax1 = plt.subplots()
        self.show_is_blocking = False

    def get_data(self):
        """Get dataframe, convert to candlesticks & return"""
        dataframe = self.grab_from_db()
        if dataframe is None:
            return None
        logger.debug('Latest 5 entries of your dataframe:')
        logger.debug(dataframe.tail(5).time)

        time_series = pd.Series(dataframe.ask_price.values, index=pd.DatetimeIndex(
            pd.to_datetime(dataframe.unixtime, unit='s'), tz=pytz.UTC))

        n_selected = len(time_series)
        if n_selected < self.latest_n:
            raise AttributeError('grab_from_db did not return enough data')

        # group the ASK prices
        group = time_series.groupby(
            pd.TimeGrouper(freq='{}Min'.format(self.group_frequency)))

        # Prepare dataframe for candlestick plotting. Needs format
        #   Date    Open    High   Low   Close
        #    XXX     123     345   123     456
        #    ...     ...    ...    ...     ...
        candles = []
        for b in group:
            try:
                candles.append(candlestick(
                    b[0],        # Date (pandas._libs.tslib.Timestamp)
                    b[1][0],     # Opening price
                    b[1].max(),  # max price
                    b[1].min(),  # min price
                    b[1][-1]     # close price
                ))
            except IndexError:
                """
                Most likely there is no data for this particular time
                  -> Skip!
                """
                pass

        # https://stackoverflow.com/questions/42437349/candlestick-plot-from-a-pandas-dataframe
        candleframe = pd.DataFrame.from_items(
            [('date',  [c.date for c in candles]),
             ('open',  [c.opn for c in candles]),
             ('high',  [c.high for c in candles]),
             ('low',   [c.low for c in candles]),
             ('close', [c.close for c in candles])])

        # if latest database entry is part of latest bin drop the last bin
        # because it's still developing
        candle_time = candleframe.ix[len(candleframe) - 1].date
        database_time = pd.to_datetime(dataframe.ix[len(dataframe) - 1].unixtime, unit='s').tz_localize('UTC')
        timedelta = database_time - candle_time
        if timedelta < pd.Timedelta(minutes=self.group_frequency):
            logger.debug('get_data: Drop last candle because time delta is only {}'
                .format(timedelta))
            candleframe.drop(candleframe.tail(1).index, inplace=True)

        # Naming columns
        candleframe.columns = ['Date', 'Open', 'High', 'Low', 'Close']

        logger.debug('get_data: Time of last candlestick: {}'
                    .format(candleframe.ix[len(candleframe) - 1].Date))
        logger.debug('get_data: Latest 10 entries before converting dates to numbers:')
        logger.debug(candleframe.tail(10))
        # Converting dates column to float values
        candleframe['Date'] = candleframe['Date'].map(mdates.date2num)
        return candles, candleframe, n_selected

    def __iter__(self):
        return self

    def __next__(self):
        """Wait for new bin. Return new data when available"""
        while True:
            try:
                data = self.get_data()
            except AttributeError:
                raise StopIteration
            if data is None:
                raise StopIteration
            candles, candleframe, n_selected = data
            latest = candleframe.Date.values[-1]
            if latest <= self.time_latest_bin:
                logger.debug('No new data available. Sleep a bit...')
                time.sleep(self.sleep_time)
                continue
            self.time_latest_bin = latest
            break
        return candles[-1]

    def show_animated(self):
        if not self.show_is_blocking:
            plt.ioff()
            self.show_is_blocking = True
        # self.fig.autofmt_xdate()
        ani = animation.FuncAnimation(self.fig, self.draw_plots,
                                      interval=1000. * self.update_freq)
        # animate(0)
        # plt.tight_layout()
        plt.show()

    def show(self):
        # TODO: Does not work as intended.
        #       Window becomes irresponsive when used in cct.py
        if self.show_is_blocking:
            plt.ion()
            plt.show()
            self.show_is_blocking = False
        self.fig.autofmt_xdate()
        self.draw_plots()
        plt.tight_layout()
        plt.draw()
        plt.pause(0.005)

    def draw_plots(self, i=0):
        candles, candleframe, n_selected = self.get_data()

        # Making plot
        # ax1 = plt.subplot2grid((1,1), (0,0), rowspan=1, colspan=1)
        self.ax1.clear()
        self.ax1.xaxis.set_major_formatter(xfmt)

        # Converts raw mdate numbers to dates
        self.ax1.xaxis_date()

        # Making candlestick plot
        # width = 1 is one full day
        candlestick_ohlc(self.ax1,
                         candleframe.values,
                         width=(1. / 24. / 60. * self.group_frequency),
                         colorup='#1ABF60',
                         colordown='#F82727',
                         alpha=0.6)
        plt.xlabel('Time of latest {} entries'.format(self.latest_n))
        plt.ylabel('{} in {}'.format(self.cryptocur, self.cur))
        plt.title('{} minute bins between {} and {}'
                  .format(self.group_frequency,
                          candles[0].date,
                          candles[-1].date))
        plt.setp(plt.gca().get_xticklabels(),
                 rotation=25,
                 horizontalalignment='right')

        logger.debug('Succesfully created candlestick plot for the latest '
                     '{} entries'.format(self.latest_n))
        logger.debug('Each candlestick corresponds to {} minutes.'
                     .format(self.group_frequency))
        logger.debug('Here are the values of the latest 10 bins:')
        logger.debug(candleframe.tail(10))

        # calculate ema
        ndatapoints = len(candleframe.Close.values)
        try:
            sma = SimpleMovingAverage(candleframe.Close.values, 55)
            ema = ExpMovingAverage(candleframe.Close.values, 20)
        except IndexError:
            logger.error('LiveTicker error: Too few data points to calculate sma/ema.')
        else:
            # draw on same axis
            self.ax1.plot(candleframe.Date.values, sma,
                          linewidth=0.75,
                          c='#293AE7',
                          alpha=0.75,
                          label='SMA 55')
            self.ax1.plot(candleframe.Date.values, ema,
                          linewidth=0.75,
                          c='#F65FC2',
                          alpha=0.75,
                          label='EMA 20')

        # zoom in because there no SMA line for the first 54 entries
        plt.xlim(candleframe.Date.values[55], candleframe.Date.values[-1])

        # decorate with some text
        # textx = candleframe.Date.values[-3]
        # texty = (0.5 *
        #          (max(candleframe.Close.values) + min(candleframe.Close.values))
        #          + 0.45 *
        #          (max(candleframe.Close.values) - min(candleframe.Close.values)))

        textx = (self.ax1.get_xlim()[0] + 0.990
                 * (self.ax1.get_xlim()[1] - self.ax1.get_xlim()[0]))
        texty = (self.ax1.get_ylim()[0] + 0.992
                 * (self.ax1.get_ylim()[1] - self.ax1.get_ylim()[0]))
        self.ax1.text(textx, texty,
                      'Refreshed {} | Latest bin {}'
                      .format(time.strftime('%H:%M:%S %Z', time.gmtime()),
                              mdates.num2date(candleframe.Date.values[-1],
                                              tz=pytz.UTC)
                              .strftime('%H:%M:%S %Z')),
                      fontsize=6, family='monospace', va='top', ha='right')
        if n_selected < self.latest_n:
            textx = (self.ax1.get_xlim()[0] + 0.01
                     * (self.ax1.get_xlim()[1] - self.ax1.get_xlim()[0]))
            # texty = ax1.get_ylim()[0] + 0.965 * (ax1.get_ylim()[1]
            #                                      - ax1.get_ylim()[0])
            self.ax1.text(textx, texty,
                          'WARNING: Too little data! '
                          '(Requested {}, got {} datapoints)'
                          .format(self.latest_n, n_selected),
                          fontsize=6, family='monospace',
                          va='top', ha='left', color='r')
        plt.legend()