import os
import krakenex
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from argparse import ArgumentParser
from cct import Kraken

def getArgs():
    '''This method parses the arguments supplied by the user during execution.'''
    parser = ArgumentParser()
    parser.add_argument('-k', '--key', default=None, help='key file for kraken')
    return parser.parse_args()

def plotRate():
    '''This method plots the rate obtained from the Kraken API using seaborn visualisation.'''
    # get arguments
    args = getArgs()
    key = os.path.join('data', 'keys', 'kraken.key')
    if args.key:
        key = args.key

    # connect to real API
    realapi = Kraken()
    realapi.load_key(key)
    realapi.conn = krakenex.Connection()

    # plot spread
    fig, axarr = plt.subplots(2, sharex=True)
    tsa, tsb = realapi.Spread({"pair": "XXRPZEUR"})
    sns.tsplot(tsa, ax=axarr[0])
    sns.tsplot(tsb, ax=axarr[1])
    tsb.plot(y='XRP in EUR', title='Bid XRP in EUR\nbetw. {} and {}'.format(tsb.first_valid_index(), tsb.last_valid_index()), ax=axarr[0])
    tsa.plot(y='XRP in EUR', title='Ask XRP in EUR\nbetw. {} and {}'.format(tsa.first_valid_index(), tsa.last_valid_index()), ax=axarr[1])
    plt.show()


if __name__ == '__main__':
    plotRate()
