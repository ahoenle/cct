import numpy as np

# https://stackoverflow.com/a/42554266/6883760
def SimpleMovingAverage(values, window):
    j = next(i for i, x in enumerate(values) if x is not None)
    our_range = range(len(values))[j + window - 1:]
    empty_list = [None] * (j + window - 1)
    sub_result = [np.mean(values[i - window + 1: i + 1]) for i in our_range]

    return np.array(empty_list + sub_result)

# https://codereview.stackexchange.com/a/166640
def ExpMovingAverage(values, window):
    """ Numpy implementation of EMA
    """
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()
    a =  np.convolve(values, weights, mode='full')[:len(values)]
    a[:window] = a[window]
    return a
