class DatabaseNotAvailable(Exception):
    """Raise if records.Database throws"""
    def __init__(self, message='', errors=[]):
        super().__init__(message)
        self.errors = errors

class RequestError(Exception):
    """Raise if request fails"""
    def __init__(self, message='', errors=[]):
        super().__init__(message)
        self.errors = errors

class ConsistencyError(Exception):
    """Raise if there's something inconsistent in data"""
    def __init__(self, message='', errors=[]):
        super().__init__(message)
        self.errors = errors
