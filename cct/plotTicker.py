import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.dates import DateFormatter
from matplotlib.finance import candlestick_ohlc
import matplotlib.dates as mdates
import pandas as pd
import seaborn as sns
import numpy as np

from auxmath import SimpleMovingAverage
from auxmath import ExpMovingAverage

##
# some settings
# TODO: config?
##

# bin size of candlesticks in candlestick plot
group_frequency = 10  # minutes
# number of data points for candlestick plots (NOT number of candlesticks)
latest_n = 10000
# AssetPair (to load TickerStore data
assetpair = 'XXBTZEUR'
# crypto currency (for correct titles)
cryptocur = 'BTC'
# real currency (for correct titles)
cur = 'EUR'

class candlestick(object):
    """Stores all information that is in one bin of the candlestick plot."""

    def __init__(self, date, opn, high, low, close):
        self.date = date
        self.opn = opn
        self.high = high
        self.low = low
        self.close = close

names = [ 'time',
          'ask_price', 'ask_whole_lot_volume', 'ask_lot_volume',
          'bid_price', 'bid_whole_lot_volume', 'bid_lot_volume',
          'last_trade_price', 'last_trade_volume',
          'volume_today', 'volume_24hrs',
          'weighted_volume_today', 'weighted_volume_24hrs',
          'ntraded_today', 'ntraded_24hrs',
          'low_today', 'low_24hrs',
          'high_today', 'high_24hrs',
          'opening_price' ]

xfmt = DateFormatter('%H:%M:%S %Z')

# TODO: hardcoded path
try:
    df = pd.read_csv('/Users/ahoenle/Software/cct/data/TickerStore_{}.csv'.
                     format(assetpair),
                     header=None, names=names)
except:
    # FIXME: Error handling
    raise

df = df.tail(latest_n)
print('Latest 10 entries of your dataframe:')
print(df.tail(10).time)

fig, ax = plt.subplots(2, sharex=True)
tsa = pd.Series(df.ask_price.values, index=pd.DatetimeIndex(df.time))
tsb = pd.Series(df.bid_price.values, index=pd.DatetimeIndex(df.time))
tsd = pd.Series(df.bid_price.values - df.ask_price.values,
                index=pd.DatetimeIndex(df.time))
# sns.tsplot(tsa, ax=axarr[0])
# sns.tsplot(tsb, ax=axarr[1])
ax[0].xaxis.set_major_formatter(xfmt)
ax[1].xaxis.set_major_formatter(xfmt)
tsb.plot(y='{} in {}'.format(cryptocur, cur),
         title='{} in {} betw. {} and {}'
         .format(cryptocur,
                 cur,
                 tsb.first_valid_index(),
                 tsb.last_valid_index()),
         c='r',
         linewidth=0.5,
         label='bid',
         ax=ax[0])
tsa.plot(y='{} in {}'.format(cryptocur, cur),
         title='{} in {} betw. {} and {}'
         .format(cryptocur,
                 cur,
                 tsb.first_valid_index(),
                 tsb.last_valid_index()),
         c='g',
         linewidth=0.5,
         label='ask',
         ax=ax[0])
tsd.plot(y='Diff in {}'.format(cur),
         c='k',
         linewidth=0.5,
         ax=ax[1])
plt.legend()
# plt.show()

# group the ASK prices
group = tsa.groupby(pd.TimeGrouper(freq='{}Min'.format(group_frequency)))

# Prepare dataframe for candlestick plotting. Needs format
#   Date    Open    High   Low   Close
#    XXX     123     345   123     456
#    ...     ...    ...    ...     ...
candles = []
for b in group:
    try:
        candles.append(candlestick(
            b[0],        # Date (pandas._libs.tslib.Timestamp)
            b[1][0],     # Opening price
            b[1].max(),  # max price
            b[1].min(),  # min price
            b[1][-1]     # close price
        ))
    except IndexError:
        """
        Most likely there is no data for this particular time
          -> Skip!
        """
        pass

# https://stackoverflow.com/questions/42437349/candlestick-plot-from-a-pandas-dataframe
candleframe = pd.DataFrame.from_items(
    [('date',  [c.date for c in candles]),
     ('open',  [c.opn for c in candles]),
     ('high',  [c.high for c in candles]),
     ('low',   [c.low for c in candles]),
     ('close', [c.close for c in candles])])

# Naming columns
candleframe.columns = ['Date','Open','High','Low','Close']

print('Latest 10 entries before converting dates to numbers:')
print(candleframe.tail(10))

# Converting dates column to float values
candleframe['Date'] = candleframe['Date'].map(mdates.date2num)

# Making plot
fig = plt.figure()
fig.autofmt_xdate()
ax1 = plt.subplot2grid((6,1), (0,0), rowspan=6, colspan=1)
ax1.xaxis.set_major_formatter(xfmt)

# Converts raw mdate numbers to dates
ax1.xaxis_date()

# Making candlestick plot
# width = 1 is one full day
candlestick_ohlc(ax1,
                 candleframe.values,
                 width=(1. / 24. / 60. * group_frequency),
                 colorup='g',
                 colordown='k',
                 alpha=0.5)
plt.xlabel('Time of latest {} entries'.format(latest_n))
plt.ylabel('{} in {}'.format(cryptocur, cur))
plt.title('{} minute bins between {} and {}'
          .format(group_frequency,
                  candles[0].date,
                  candles[-1].date))
plt.setp( plt.gca().get_xticklabels(), rotation=45, horizontalalignment='center')

print('Succesfully created candlestick plot for the latest {} entries'
      .format(latest_n))
print('Each candlestick corresponds to {} minutes.'
      .format(group_frequency))
print('Here are the values of the latest 10 bins:')
print(candleframe.tail(10))

# calculate ema
ndatapoints = len(candleframe.Close.values)
try:
    sma = SimpleMovingAverage(candleframe.Close.values, 55)
    ema = ExpMovingAverage(candleframe.Close.values, 20)
except IndexError:
    print('ERROR: Too few data points to calculate sma / ema.')
else:
    # draw on same axis
    ax1.plot(candleframe.Date.values, sma,
             linewidth=0.75,
             c='#bd6966',
             alpha=0.75,
             label='SMA 55')
    ax1.plot(candleframe.Date.values, ema,
             linewidth=0.75,
             c='#8352a8',
             alpha=0.75,
             label='EMA 20')

plt.legend()
plt.show()
