import time


class Clock(object):
    """Clock to sync crytpo trader, server and dataset times.

    Starts a clock from a given unix time and counts the time that has
    passed.
    May be run with a multiplier to run faster.

    Attributes:
        None
    """
    def __init__(self, t0, multiplier=1):
        """Initialize with t0 and multiplier.

        Args:
            t0: Start time
            multiplier: Run clock (multiplier) times faster than real clock

        Raises:
            TypeError: If t0 cannot be converted
        """
        if isinstance(t0, time.struct_time):
            self.t0 = time.mktime(t0)
        elif isinstance(t0, int):
            # interpret as time since epoch
            self.t0 = t0
        elif isinstance(t0, str):
            try:
                self.t0 = time.mktime(time.strptime(t0, '%Y-%m-%d'))
            except:
                raise TypeError('t0 ({}) cannot be converted to %Y-%m-%d'
                                .format(t0))
        else:
            raise TypeError('Wrong format of t0 ({})'.format(type(t0)))
        self.unixt0 = time.time()
        self.mx = multiplier
        print("Initialized clock.")
        print("Set time   {}"
              .format(time.strftime('%a %d %b %Y %H:%M:%S %Z',
                                    time.localtime(self.t0))))
        print("Time now   {}"
              .format(time.strftime('%a %d %b %Y %H:%M:%S %Z',
                                    time.localtime(self.unixt0))))
        print("Multiplier {}".format(self.mx))

    def time(self):
        """Return time "now".

        Args:
            None
        """
        dt = time.time() - self.unixt0
        dt = self.mx * dt
        faketime = self.t0 + dt
        return faketime

    def strtime(self):
        """Return time "now" as a formatted string.

        Args:
            None
        """
        t = self.time()
        return time.strftime('%a %d %b %Y %H:%M:%S %Z',
                             time.localtime(t))


if __name__ == '__main__':
    c = Clock(1505078961, 100)
    print('Sleeping 10 seconds...')
    time.sleep(10)
    print('Asking for time ' + c.strtime())
