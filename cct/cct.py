#!/usr/bin/env python

import krakenex
import os
import pandas as pd
import time
from datetime import datetime

from Kraken import Kraken
from liveTicker import LiveTicker
from fakeLiveTicker import FakeLiveTicker
from strategies import ema_sma

# set up the logger
import logging
logger = logging.getLogger('cct')
logger.setLevel(logging.INFO)
# create file handler which logs even debug messages
fh = logging.FileHandler('cct.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
class MultiLineFormatter(logging.Formatter):
    def format(self, record):
        str = logging.Formatter.format(self, record)
        header, footer = str.split(record.message)
        str = str.replace('\n', '\n' + ' '*len(header))
        return str
formatter = MultiLineFormatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

# realapi = krakenex.API()
realapi = Kraken()
realapi.load_key(os.path.join(os.environ['CCT'], 'data', 'keys', 'kraken.key'))
realapi.conn = krakenex.Connection()

fakeapi = Kraken()
fakeapi.uri = 'https://127.0.0.1:4443'
# TODO: Support for Key in fake API
# fakeapi.load_key(os.path.join(os.environ['CCT'], 'data', 'keys', 'kraken.key'))
fakeapi.conn = krakenex.Connection(uri='127.0.0.1:4443')
from http.client import HTTPSConnection
import ssl
fakeapi.conn.conn = HTTPSConnection('127.0.0.1:4443',
                                    timeout=30,
                                    context=ssl._create_unverified_context())


class CryptoCurrencyTrader(object):
    """Trade crypto currencies

    * api connection
    * liveTicker
    * strategy
    * if new event in liveticker ask strategy what to do

    Attributes:
        #WRITEME

    """

    def __init__(self, api, strategy, liveticker, candle_init=None):
        self.api = api
        self.strategy = strategy
        self.liveticker = liveticker

    def run(self):
        for i, candle in enumerate(self.liveticker):
            # showing the liveticker makes program instable
            # self.liveticker.show()
            logger.info('Got {}'.format(candle))
            self.strategy.process(candle)
            if i % 60 == 0:
                self.strategy.plot(width = 1. / 24. / 60. 
                    * self.liveticker.group_frequency)
        logger.error('Out of ticker data.')
        self.strategy.plot(width = 1. / 24. / 60. 
            * self.liveticker.group_frequency)
        self.strategy.summary(blocking=False)

# ticker = LiveTicker()
ticker = FakeLiveTicker(input_db='/Users/ahoenle/Software/cct/databases/TickerStore/2017/10/31.sqlite3')
candles, candleframe, n_selected = ticker.get_data()
# Profiling
# import cProfile
# cProfile.run('ticker.grab_data()', 'grab_data.profile')
# exit(0)

strategy = ema_sma.ema_sma(market_api = fakeapi,
                           max_risk = 5,      # EUR
                           entry_risk = 0.1,  # percent of max_risk
                           max_invest = 10,   # EUR
                           candle_init=candles)

testTrader = CryptoCurrencyTrader(fakeapi, strategy, ticker)
testTrader.run()
