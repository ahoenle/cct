import time
import krakenex
import json

from CCTExceptions import *

class Kraken(krakenex.API):
    """
        **** WRITEME ****
    """
    def Spread(self, pairs=[]):
        """Run Spread request

        Args:
            pairs: List of asset pairs

        Returns:
            List of two pandas.Series for ask and bid prices

        Raises:
            RequestError: If request fails
        """
        args = {'pair': pairs}
        ret = self._json(self.query_public('Spread', args))
        if ret['error']:
            raise RequestError('Spread request failed {}'
                               .format(ret['error']))
        spread = ret['result'][args['pair']]
        time_indices = [self.to_datetime(e[0]) for e in spread]
        a = [ float(x[2]) for x in spread ]
        b = [ float(x[1]) for x in spread ]
        tsa = pd.Series(a, index=time_indices) # ask
        tsb = pd.Series(b, index=time_indices) # bid
        return [tsa, tsb]


    def Ticker(self, pairs):
        """Get ticker for an asset pair.

        Args:
            pairs: List of asset pairs

        Returns:
            Ticker information for asset pairs.

        Raises:
            RequestError: If request fails
        """
        args = {'pair': pairs}
        # ret = self._json(self.query_public('Ticker', args))
        ret = self._json(self.query_public('Ticker', {'pair': pairs}))
        if ret['error']:
            print(ret)
            print(ret['error'])
            raise RequestError('Ticker request failed {}'
                               .format(ret['error']))
        return ret['result']

    def _json(self, obj):
        """Decode with json if it's a string. Otherwise do nothing.

        Args:
            obj: Some object. Possibly alredy converted by JSON.

        Returns:
            JSON decoded object with content of obj

        Raises:
            If JSON conversion rises.
        """
        if isinstance(obj, str):
            return json.loads(obj)
        print('Kraken._json: calling me was unnecessary.')
        return obj

    def unixtime(self):
        """Get time from Kraken Server

        Args:
            None

        Returns:
            A floating point number, which is the time since the epoch.

        Raises:
            RequestError: If the request returns any errors.
        """
        ret = self._json(self.query_public('Time'))
        if ret['error']:
            raise RequestError('Time request failed {}'
                               .format(ret['error']))
        unix = ret['result']['unixtime']
        return int(unix)

    def strtime(self):
        t = self.unixtime()
        return time.strftime('%a %d %b %Y %H:%M:%S %Z',
                             time.localtime(t))

    def to_datetime(self, t):
        ltime = time.localtime(t)
        dtime = datetime(ltime.tm_year, ltime.tm_mon, ltime.tm_mday,
                         ltime.tm_hour, ltime.tm_min, ltime.tm_sec)
        return pd.to_datetime(dtime)
