import records
import time
import os

from auxtools import TickerInfo
from clock import Clock
from Kraken import Kraken

ALL_ASSET_PAIRS = ['BCHEUR', 'BCHUSD', 'BCHXBT', 'DASHEUR', 'DASHUSD', 'DASHXBT',
                   'EOSETH', 'EOSXBT', 'GNOETH', 'GNOXBT', 'USDTZUSD',
                   'XETCXETH', 'XETCXXBT', 'XETCZEUR', 'XETCZUSD', 'XETHXXBT',
                   'XETHZCAD', 'XETHZEUR', 'XETHZGBP', 'XETHZJPY', 'XETHZUSD',
                   'XICNXETH', 'XICNXXBT', 'XLTCXXBT', 'XLTCZEUR', 'XLTCZUSD',
                   'XMLNXETH', 'XMLNXXBT', 'XREPXETH', 'XREPXXBT', 'XREPZEUR',
                   'XXBTZCAD', 'XXBTZEUR', 'XXBTZGBP', 'XXBTZJPY', 'XXBTZUSD',
                   'XXDGXXBT', 'XXLMXXBT', 'XXMRXXBT', 'XXMRZEUR', 'XXMRZUSD',
                   'XXRPXXBT', 'XXRPZEUR', 'XXRPZUSD', 'XZECXXBT', 'XZECZEUR',
                   'XZECZUSD']



class TickerStore(object):
    """Get Kraken Ticker and store it away.

    Attributes:
        api: Kraken API to perform requests
        interval: Time interval (in seconds) for Ticker requests
        ncalls: Internal counter for time calibration
        clock: Kraken server synced clock
        database: Records connection to SQLite DB
        db_date: database day (of month)
    """

    def __init__(self, pair='ALL'):
        if pair == 'ALL':
            pair = ALL_ASSET_PAIRS
        self.pairs = pair
        self.api = Kraken()
        self.ncalls = 0
        self.interval = 5
        self.clock = None
        self.database = None
        print('TickerStore - Initialization done for {}'.format(self.pairs))

    def calibrate_time(self):
        """Calibrate the time from Kraken server.

        Only every 500th call counts.
        """
        self.ncalls += 1
        if self.clock and self.ncalls < 500:
            return
        print('Calibrating time...')
        t0 = self.api.unixtime()
        self.clock = Clock(t0, multiplier=1)
        self.ncalls = 0
        print('Calibration done.')

    def refresh_db(self):
        """Close the current database connection and open up a new one.

        Name is given by the current date.
        """
        if self.database:
            self.database.close()
        t = time.gmtime()
        db_out_dir = os.path.join(os.environ['CCT'],
                                  'databases', 'TickerStore',
                                  str(t.tm_year), str(t.tm_mon))
        try:
            os.makedirs(db_out_dir)
        except OSError:
            # exists
            pass

        self.db_day = t.tm_mday
        db_out = os.path.join(db_out_dir, '{}.sqlite3'.format(t.tm_mday))
        self.database = records.Database('sqlite:///{}'.format(db_out))

    def run(self):
        print('TickerStore - Running forever...')
        self.refresh_db()
        while True:
            self.calibrate_time()
            if time.gmtime(self.clock.time()).tm_mday != self.db_day:
                self.refresh_db()
            # make time in TickerInfo independet of how long the request took
            if isinstance(self.pairs, list):
                try:
                    ticker_message = self.api.Ticker(pairs=', '.join(self.pairs))
                except socket.timeout:
                    time.sleep(30)
                    continue
                t = self.clock.strtime()
                for assetpair in self.pairs:
                    ticker = TickerInfo(t, ticker_message[assetpair])
                    ticker.writedb(assetpair, self.database)
            else:
                try:
                    ticker_message = self.api.Ticker(pairs=self.pairs)
                except socket.timeout:
                    time.sleep(30)
                    continue
                ticker = TickerInfo(t, ticker_message[self.pairs])
                t = self.clock.strtime()
                ticker.writedb(self.pairs, self.database)
            time.sleep(self.interval)

# if __name__ == '__main__':
#     import sys
#     if len(sys.argv) > 1:
#         pair = sys.argv[1]
#     else:
#         pair = 'XXRPZEUR'
#     ts = TickerStore(pair)
#     ts.run()

# check CCT environment variable
try:
    os.environ['CCT']
except KeyError:
    print('Error "CCT" environment variable (local cct path) not set.')
else:
    ts = TickerStore()
    ts.run()
