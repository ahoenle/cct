from auxmath import SimpleMovingAverage
from auxmath import ExpMovingAverage
import numpy as np
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter
from matplotlib.finance import candlestick_ohlc
import matplotlib.pyplot as plt
import pandas as pd
import pytz
import seaborn as sns
import time
import logging
logger = logging.getLogger('cct.strategies.ema_sma')

class short_option(object):
    """
        *** WRITE ME ****
    """
    def __init__(self, volume, bid_price, close_at, buy_time):
        self.volume = volume
        self.bid_price = bid_price
        self.close_at = close_at
        self.buy_time = buy_time
        self.buy_time_str = time.strftime('%a %d %b %Y %H:%M:%S %Z',
                                          time.localtime(int(self.buy_time)))

    def strtime(self):
        return

    def __str__(self):
        s = ('<Short option bought on {0} with volume {1} at bid price {2}. '
             'To be closed under {3}>'
             .format(self.buy_time_str, self.volume,
                     self.bid_price, self.close_at))
        return s

    def __repr__(self):
        return self.__str__()


class ema_sma(object):
    """
    Watch: https://www.youtube.com/watch?v=VQaiN_kWQdg

    Buys and sells short positions.

    Attributes:
        max_risk: Maximum amount (EUR) that is invested in risky positions, i.e.
                  in developing trends
        entry_risk: Percentage (PER OPTION) of max_risk that is spent
                    on newly developing trends (default: 0.1).
        max_invest: Maximum amount (EUR) that is invested at the same time.
                    Can be > max_risk when earlier aquired options are already
                    profitable.
        candles: all candles, based on which decisions happen
        options: list of all aquired options
        safety_margin: Minimum relative ema-sma distance (default: 0.15)
    """
    def __init__(self, market_api, max_risk=5, entry_risk=0.1, max_invest=10, candle_init=None):
        self.api = market_api
        self.max_risk = max_risk
        self.entry_risk = entry_risk
        self.max_invest = max_invest
        self.options = []
        self.safety_margin = 0.03  # safey margin (abs. value)
        if candle_init:
            self.candles = candle_init
            logger.info('Initialized candle list with length {}'.format(len(self.candles)))
        else:
            self.candles = []
        self.max_n_options = int(1. / entry_risk)
        self.naquired = 0
        self.nclosed = 0
        self.init_meta()

    def __reps__(self):
        return self.__str__()

    def __str__(self):
        from pprint import pformat
        vars_to_print = dict(vars(self))
        del vars_to_print['candles']
        del vars_to_print['meta']
        del vars_to_print['ax']
        del vars_to_print['fig']
        s = ("<"
             + type(self).__name__
             + "> " + pformat(vars_to_print, indent=2, width=50))
        # s = '<ema_sma: '
        # for key, val in vars_to_print.items():
        #     s += '{}: {}'.format(key, val)
        #     s += ', '
        # s = s.rstrip(', ')
        # s += '>'
        return s

    def init_meta(self):
        """Initialize a dictionary with meta data for trades"""
        self.meta = {
            'profit': [],
            'relative_profit': [],
            'lifetime': []
        }

    def check_and_close(self, candle):
        """Check all aquired options and close if needed"""
        current = candle.close
        for idx, opt in enumerate(self.options):
            if opt.close_at >= current:
                closed_opt = self.close(idx)
                profit = current - closed_opt.bid_price
                relative_profit = current / closed_opt.bid_price
                self.meta['profit'].append(profit)
                self.meta['relative_profit'].append(relative_profit)
                self.meta['lifetime'].append(self.api.unixtime() - closed_opt.buy_time)
                logger.info('Closed {} at {} for {}. Profit = {}'
                            .format(closed_opt.buy_time_str, self.api.strtime(),
                                    current, profit))

    def acquire(self, candle, close_at):
        new_option = short_option(volume = (self.entry_risk
                                            * self.max_risk),
                                  bid_price = candle.close,
                                  close_at = close_at,
                                  buy_time = self.api.unixtime())
        self.naquired += 1
        self.options.append(new_option)
        """
            *** API STUFF TO ACQUIRE IT ***
        """
        return new_option

    def close(self, idx):
        opt = self.options[idx]
        """
           *** API STUFF TO SELL IT ***
        """
        self.nclosed += 1
        # pop after all is done successfully
        return self.options.pop(idx)

    def process(self, candle):
        """Process a new data bin (candlestick)"""
        logger.info('ema_sma status')
        logger.info(self)
        self.candles.append(candle)
        if len(self.candles) < 55:
            logger.warn('ema_sma: not enough data to perform any action')
            return
        close_vals = [c.close for c in self.candles]
        sma = SimpleMovingAverage(close_vals, 55)
        ema = ExpMovingAverage(close_vals, 20)
        self.check_and_close(candle)
        if ema[-1] > sma[-1]:
            # don't buy during positive trends
            logger.info('ema_sma: ema > sma - no action')
            return
        relative_difference = 100. * abs(ema[-1] - sma[-1]) / (ema[-1] + sma[-1])
        if relative_difference < self.safety_margin:
            # don't buy in instable times
            logger.info('ema_sma: relative difference < margin ({0:.4f} < {1:.4f})'.
                  format(relative_difference, self.safety_margin))
            return
            # update improved close_at to all existing options
        if (candle.high >= ema[-1] and candle.opn < ema[-1] and candle.close < ema[-1]):
            for opt in self.options:
                if sma[-1] < opt.close_at:
                    logger.info('Updating close_at of option from {}'
                                .format(opt.buy_time_str))
                    logger.info('  {} --> {}'.format(opt.close_at, sma[-1]))
                    opt.close_at = sma[-1]
            # check if there is risk left to aquire new options
            if len(self.options) >= self.max_n_options:
                logger.info('ema_sma: no risk left to buy additional options')
            else:
                # aquire new option
                new_option = self.acquire(candle, close_at=sma[-1])
                logger.info('ema_sma: Aquired new option')
                logger.info(new_option)
        logger.info('ema_sma: **update** status')
        logger.info(self)

    def plot(self, width):
        """Plot all current candles, ema & sma curves & buys and sells"""
        # fig = plt.figure()
        if not hasattr(self, 'fig'):
            self.fig, self.ax = plt.subplots()
            plt.ion()    
            plt.show()
        xfmt = DateFormatter('%d.%m. - %H:%M:%S %Z')
        sns.set_style('whitegrid', {'grid-color': '1.0'})

        ###
        #     *** TODO ***
        # This is copy&paste from liveTickerBase
        # Could we wrap this into a plotting class??
        ###

        # Making plot
        # self.ax = plt.subplot2grid((1,1), (0,0), rowspan=1, colspan=1)
        self.ax.clear()
        self.ax.xaxis.set_major_formatter(xfmt)

        # Converts raw mdate numbers to dates
        self.ax.xaxis_date()

        candleframe = pd.DataFrame.from_items(
            [('date',  [c.date for c in self.candles]),
             ('open',  [c.opn for c in self.candles]),
             ('high',  [c.high for c in self.candles]),
             ('low',   [c.low for c in self.candles]),
             ('close', [c.close for c in self.candles])])
        # Naming columns
        candleframe.columns = ['Date','Open','High','Low','Close']
        candleframe['Date'] = candleframe['Date'].map(mdates.date2num)
        candlestick_ohlc(self.ax,
                         candleframe.values,
                         width=width,
                         colorup='#1ABF60',
                         colordown='#F82727',
                         alpha=0.6)
        plt.xlabel('Time of latest candles')
        plt.setp(plt.gca().get_xticklabels(),
                 rotation=25,
                 horizontalalignment='right')

        # calculate ema
        try:
            sma = SimpleMovingAverage(candleframe.Close.values, 55)
            ema = ExpMovingAverage(candleframe.Close.values, 20)
        except IndexError:
            logger.error('LiveTicker error: Too few data points to calculate sma/ema.')
        else:
            # draw on same axis
            self.ax.plot(candleframe.Date.values, sma,
                     linewidth=0.75,
                     c='#293AE7',
                     alpha=0.75,
                     label='SMA 55')
            self.ax.plot(candleframe.Date.values, ema,
                     linewidth=0.75,
                     c='#F65FC2',
                     alpha=0.75,
                     label='EMA 20')

        # zoom in because there no SMA line for the first 54 entries
        xlow_idx = max(55, len(candleframe.Date.values) - 100)
        plt.xlim(candleframe.Date.values[xlow_idx], candleframe.Date.values[-1])

        # decorate with some text
        # textx = candleframe.Date.values[-3]
        # texty = (0.5 *
        #          (max(candleframe.Close.values) + min(candleframe.Close.values))
        #          + 0.45 *
        #          (max(candleframe.Close.values) - min(candleframe.Close.values)))

        textx = (self.ax.get_xlim()[0] + 0.990
                 * (self.ax.get_xlim()[1] - self.ax.get_xlim()[0]))
        texty = (self.ax.get_ylim()[0] + 0.992
                 * (self.ax.get_ylim()[1] - self.ax.get_ylim()[0]))
        self.ax.text(textx, texty,
                      'Refreshed {} | Latest bin {}'
                      .format(time.strftime('%H:%M:%S %Z', time.gmtime()),
                              mdates.num2date(candleframe.Date.values[-1],
                                              tz=pytz.UTC)
                              .strftime('%H:%M:%S %Z')),
                 fontsize=6, family='monospace', va='top', ha='right')
        plt.legend()
        plt.tight_layout()
        plt.draw()
        plt.pause(0.005)

    def summary(self, blocking=False):
        logger.info('ema_sma summary')
        logger.info('Number of aquired options:       {}'.format(self.naquired))
        logger.info('Number of closed options:        {}'.format(self.nclosed))
        logger.info('Total profit:                    {}'.format(np.sum(self.meta['profit'])))
        logger.info('Average sell / buy price ratio:  {}'.format(np.average(self.meta['relative_profit'])))
        logger.info('Average option lifetime:         {}'.format(np.average(self.meta['lifetime'])))
        logger.info('Number of remaining options:     {}'.format(len(self.options)))
        if blocking:
            import code
            code.interact(local=locals())
