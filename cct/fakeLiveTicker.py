import datetime as dt
import time
import matplotlib
import matplotlib.animation as animation
import matplotlib.ticker as mticker
from matplotlib.dates import DateFormatter
from matplotlib.finance import candlestick_ohlc
import matplotlib.dates as mdates
import pandas as pd
import records
import sqlalchemy
import numpy as np
import os
import pytz

import logging
# create logger
logger = logging.getLogger('cct.fakeLiveTicker')
module_logger = logging.getLogger('fakeLiveTicker')
module_logger.setLevel(logging.INFO)
# create file handler which logs even debug messages
fh = logging.FileHandler('cct.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the module_logger
module_logger.addHandler(fh)
module_logger.addHandler(ch)

from liveTickerBase import LiveTickerBase


class FakeLiveTicker(LiveTickerBase):
    """
       WRITEME
    """

    def __init__(self, input_db, group_frequency=2, latest_n=3000,
                 assetpair='XXRPZEUR', cryptocur='XRP', cur='EUR',
                 update_freq=0.1):
        super(FakeLiveTicker, self).__init__(group_frequency, latest_n,
                                             assetpair, cryptocur, cur, update_freq)
        self.setup_db(input_db)
        self.current_start_index = 0
        self.sleep_time = 0

    def setup_db(self, db_file):
        """Setup the records db from db_file
        """
        try:
            self.db = records.Database('sqlite:///{}'.format(db_file))
        except sqlalchemy.exc.OperationalError:
            raise DatabaseNotAvailable(
                'Are you sure the database exists under: {}?'
                .format(db_file))
        # load the whole thing
        self.db_content = self.db.query('SELECT * FROM'
                                        ' (SELECT * FROM {} ORDER BY time DESC)'
                                        'sub ORDER BY unixtime'
                                        .format(self.assetpair))
        logger.info('setup_db: Selected {} entries from db {}.'
                    .format(len(self.db_content.all()), db_file))

    def grab_from_db(self):
        """Grab data from buffered db and return as dataframe.
        """
        # start_time = self.db_content[self.current_index]['unixtime']
        # while True:
        #     selected_time = self.db_content[self.current_index]['unixtime']
        #     timediff = selected_time - start_time
        #     if timediff > self.group_frequency * 60:
        #         break
        #     try:
        #         db_subset.append(self.db_content[self.current_index])
        #     except KeyError:
        #         return None
        #     self.current_index += 1
        try:
            db_subset = self.db_content[self.current_start_index:self.current_start_index + self.latest_n]
        except KeyError:
            return None
        else:
            self.current_start_index += 3 * self.group_frequency
            logger.debug('grab_from_db: Updated current_start_index = {}'.format(self.current_start_index))
        dataframe = db_subset.export('df')
        logger.debug('grab_from_db: Time of last element: {}'
            .format(dataframe.ix[len(dataframe) - 1].time))
        return dataframe

if __name__ == '__main__':
    logger = module_logger
    ticker = FakeLiveTicker(input_db='/Users/ahoenle/Software/cct/databases/TickerStore/2017/10/27.sqlite3')
    ticker.show_animated()
