import csv
import time
import os
import progressbar
import records
import sqlalchemy

ALL_ASSET_PAIRS = ['BCHEUR', 'BCHUSD', 'BCHXBT', 'DASHEUR', 'DASHUSD', 'DASHXBT',
                   'EOSETH', 'EOSXBT', 'GNOETH', 'GNOXBT', 'USDTZUSD',
                   'XETCXETH', 'XETCXXBT', 'XETCZEUR', 'XETCZUSD', 'XETHXXBT',
                   'XETHZCAD', 'XETHZEUR', 'XETHZGBP', 'XETHZJPY', 'XETHZUSD',
                   'XICNXETH', 'XICNXXBT', 'XLTCXXBT', 'XLTCZEUR', 'XLTCZUSD',
                   'XMLNXETH', 'XMLNXXBT', 'XREPXETH', 'XREPXXBT', 'XREPZEUR',
                   'XXBTZCAD', 'XXBTZEUR', 'XXBTZGBP', 'XXBTZJPY', 'XXBTZUSD',
                   'XXDGXXBT', 'XXLMXXBT', 'XXMRXXBT', 'XXMRZEUR', 'XXMRZUSD',
                   'XXRPXXBT', 'XXRPZEUR', 'XXRPZUSD', 'XZECXXBT', 'XZECZEUR',
                   'XZECZUSD']

names = [ 'unixtime', 'time',
          'ask_price', 'ask_whole_lot_volume', 'ask_lot_volume',
          'bid_price', 'bid_whole_lot_volume', 'bid_lot_volume',
          'last_trade_price', 'last_trade_volume',
          'volume_today', 'volume_24hrs',
          'weighted_volume_today', 'weighted_volume_24hrs',
          'ntraded_today', 'ntraded_24hrs',
          'low_today', 'low_24hrs',
          'high_today', 'high_24hrs',
          'opening_price' ]


class candlestick(object):
    """Stores all information that is in one bin of the candlestick plot."""

    def __init__(self, date, opn, high, low, close):
        self.date = date
        self.opn = opn
        self.high = high
        self.low = low
        self.close = close
        if opn < close:
            self.category = 'green'
        else:
            self.category = 'red'

    def __repr__(self):
        return '<candlestick at 0x{0:x}>'.format(id(self))

    def __str__(self):
        s = ('<candlestick at 0x{0:x}. '
             'date: {1.date}, category: {1.category}, open: {1.opn}, '
             'low: {1.low}, high: {1.high}, close: {1.close}>'
             .format(id(self), self))
        return s


class TickerInfo(object):
    """
    Attributes:
        unixtime: floating point number, time since epoch
        time: string formatted time (with timezone information)
        a: ask array(<price>, <whole lot volume>, <lot volume>),
        b: bid array(<price>, <whole lot volume>, <lot volume>),
        c: last trade closed array(<price>, <lot volume>),
        v: volume array(<today>, <last 24 hours>),
        p: volume weighted average price array(<today>, <last 24 hours>),
        t: number of trades array(<today>, <last 24 hours>),
        l: low array(<today>, <last 24 hours>),
        h: high array(<today>, <last 24 hours>),
        o: today's opening price
    """

    def __init__(self, timestr = None, d = None):
        """
        Args:
            d: A dictionary from a Ticker request
        """
        if timestr:
            self.unixtime = time.mktime(
                time.strptime(timestr, '%a %d %b %Y %H:%M:%S %Z'))
            self.time = timestr
        if d:
            self.a = d['a']
            self.b = d['b']
            self.c = d['c']
            self.v = d['v']
            self.p = d['p']
            self.t = d['t']
            self.l = d['l']
            self.h = d['h']
            self.o = d['o']

    def as_csv(self):
        csv = ('{0}, {1}'
               '{2[0]}, {2[1]}, {2[2]}, '
               '{3[0]}, {3[1]}, {3[2]}, '
               '{4[0]}, {4[1]}, '
               '{5[0]}, {5[1]}, '
               '{6[0]}, {6[1]}, '
               '{7[0]}, {7[1]}, '
               '{8[0]}, {8[1]}, '
               '{9[0]}, {9[1]}, '
               '{10}'
               .format(self.unixtime, self.time,
                       self.a, self.b, self.c, self.v, self.p,
                       self.t, self.l, self.h, self.o))
        return csv

    def from_csv(self, csv_arr):
        self.unixtime = csv_arr[0]
        self.time = csv_arr[1]
        self.a = [float(csv_arr[2]),  float(csv_arr[3]),  float(csv_arr[4])]
        self.b = [float(csv_arr[5]),  float(csv_arr[6]),  float(csv_arr[7])]
        self.c = [float(csv_arr[8]),  float(csv_arr[8])]
        self.v = [float(csv_arr[10]), float(csv_arr[11])]
        self.p = [float(csv_arr[12]), float(csv_arr[13])]
        self.t = [float(csv_arr[14]), float(csv_arr[15])]
        self.l = [float(csv_arr[16]), float(csv_arr[17])]
        self.h = [float(csv_arr[18]), float(csv_arr[19])]
        self.o = float(csv_arr[20])
        return self

    def writecsv(self, assetpair):
        with open(os.path.join(
                os.environ['CCT'], 'data',
                'TickerStore_' + assetpair + '.csv'), 'a') as f:
            f.write(self.as_csv())
            f.write('\n')

    def writedb(self, assetpair, db):
        db_header = 'unixtime NUMERIC PRIMARY KEY, time TEXT, '
        for eachName in names[2:]:
            db_header += '{} NUMERIC, '.format(eachName)
        db_header = db_header.rstrip(', ')
        db.query('CREATE TABLE IF NOT EXISTS {} ({})'
                 .format(assetpair, db_header))
        db.query('INSERT INTO {} ('
                 'unixtime, time,'
                 'ask_price, ask_whole_lot_volume, ask_lot_volume,'
                 'bid_price, bid_whole_lot_volume, bid_lot_volume,'
                 'last_trade_price, last_trade_volume,'
                 'volume_today, volume_24hrs,'
                 'weighted_volume_today, weighted_volume_24hrs,'
                 'ntraded_today, ntraded_24hrs,'
                 'low_today, low_24hrs,'
                 'high_today, high_24hrs,'
                 'opening_price'
                 ')'
                 'VALUES('
                 ':unixtime, :timestr,'
                 ':ask_price, :ask_whole_lot_volume, :ask_lot_volume,'
                 ':bid_price, :bid_whole_lot_volume, :bid_lot_volume,'
                 ':last_trade_price, :last_trade_volume,'
                 ':volume_today, :volume_24hrs,'
                 ':weighted_volume_today, :weighted_volume_24hrs,'
                 ':ntraded_today, :ntraded_24hrs,'
                 ':low_today, :low_24hrs,'
                 ':high_today, :high_24hrs,'
                 ':opening_price'
                 ')'.format(assetpair),
                 unixtime              = self.unixtime,
                 timestr               = self.time,
                 ask_price             = self.a[0],
                 ask_whole_lot_volume  = self.a[1],
                 ask_lot_volume        = self.a[2],
                 bid_price             = self.b[0],
                 bid_whole_lot_volume  = self.b[1],
                 bid_lot_volume        = self.b[2],
                 last_trade_price      = self.c[0],
                 last_trade_volume     = self.c[1],
                 volume_today          = self.v[0],
                 volume_24hrs          = self.v[1],
                 weighted_volume_today = self.p[0],
                 weighted_volume_24hrs = self.p[1],
                 ntraded_today         = self.t[0],
                 ntraded_24hrs         = self.t[1],
                 low_today             = self.l[0],
                 low_24hrs             = self.l[1],
                 high_today            = self.h[0],
                 high_24hrs            = self.h[1],
                 opening_price         = self.o)


def csvtosqlite(db_out = None):
    if not db_out:
        try:
            os.environ['CCT']
        except KeyError:
            print('You must specify the environment variable "CCT".')
            return
        else:
            db_out = os.path.join(os.environ['CCT'], 'data', 'TickerStore.db')
        db = records.Database('sqlite:///{}'.format(db_out))
        db_header = 'unixtime NUMERIC PRIMARY KEY, time TEXT, '
        for eachName in names[2:]:
            db_header += '{} NUMERIC, '.format(eachName)
        db_header = db_header.rstrip(', ')
        for i, assetpair in enumerate(ALL_ASSET_PAIRS, start=1):
            print('\nConverting asset pair {}/{} ({})'
                  .format(i, len(ALL_ASSET_PAIRS),assetpair))
            db.query('CREATE TABLE IF NOT EXISTS {} ({})'
                     .format(assetpair, db_header))
            # db.query('CREATE TABLE {} ({})'.format(assetpair, db_header))
            with open(os.path.join(os.environ['CCT'], 'data',
                                   'moredata',
                                   'TickerStore_{}.csv'
                                   .format(assetpair))) as f:
                numlines = len(f.readlines())
            csv_data = csv.reader(open(os.path.join(os.environ['CCT'], 'data',
                                                    'moredata',
                                                    'TickerStore_{}.csv'
                                                    .format(assetpair))))
            bar = progressbar.ProgressBar(max_value = numlines)
            for j, row in enumerate(csv_data, start=1):
                bar.update(j)
                ti = TickerInfo().from_csv(row)
                try:
                    db.query('INSERT INTO {} ('
                             'unixtime, time,'
                             'ask_price, ask_whole_lot_volume, ask_lot_volume,'
                             'bid_price, bid_whole_lot_volume, bid_lot_volume,'
                             'last_trade_price, last_trade_volume,'
                             'volume_today, volume_24hrs,'
                             'weighted_volume_today, weighted_volume_24hrs,'
                             'ntraded_today, ntraded_24hrs,'
                             'low_today, low_24hrs,'
                             'high_today, high_24hrs,'
                             'opening_price'
                             ')'
                             'VALUES('
                             ':unixtime, :timestr,'
                             ':ask_price, :ask_whole_lot_volume, :ask_lot_volume,'
                             ':bid_price, :bid_whole_lot_volume, :bid_lot_volume,'
                             ':last_trade_price, :last_trade_volume,'
                             ':volume_today, :volume_24hrs,'
                             ':weighted_volume_today, :weighted_volume_24hrs,'
                             ':ntraded_today, :ntraded_24hrs,'
                             ':low_today, :low_24hrs,'
                             ':high_today, :high_24hrs,'
                             ':opening_price'
                             ')'.format(assetpair),
                             unixtime              = ti.unixtime,
                             timestr               = ti.time,
                             ask_price             = ti.a[0],
                             ask_whole_lot_volume  = ti.a[1],
                             ask_lot_volume        = ti.a[2],
                             bid_price             = ti.b[0],
                             bid_whole_lot_volume  = ti.b[1],
                             bid_lot_volume        = ti.b[2],
                             last_trade_price      = ti.c[0],
                             last_trade_volume     = ti.c[1],
                             volume_today          = ti.v[0],
                             volume_24hrs          = ti.v[1],
                             weighted_volume_today = ti.p[0],
                             weighted_volume_24hrs = ti.p[1],
                             ntraded_today         = ti.t[0],
                             ntraded_24hrs         = ti.t[1],
                             low_today             = ti.l[0],
                             low_24hrs             = ti.l[1],
                             high_today            = ti.h[0],
                             high_24hrs            = ti.h[1],
                             opening_price         = ti.o)
                except sqlalchemy.exc.IntegrityError:
                    """
                      entry exists
                    """
                    pass

def csvinsertmtime():
    """Insert time as floating point number as first column"""
    for i, assetpair in enumerate(ALL_ASSET_PAIRS, start=1):
        print('\nUpdating TickerStore {}/{} ({})'
              .format(i, len(ALL_ASSET_PAIRS), assetpair))
        csvfile = os.path.join(os.environ['CCT'], 'data',
                               'moredata',
                               'TickerStore_{}.csv'
                               .format(assetpair))
        csvout = csvfile.replace('.csv', '.out.csv')
        with open(csvfile) as f:
            numlines = len(f.readlines())
        bar = progressbar.ProgressBar(max_value = numlines)
        with open(csvfile) as fread, \
             open(csvout, 'w') as fwrite:
            reader = csv.reader(fread)
            writer = csv.writer(fwrite, delimiter=',')
            # writer.writeheader()
            for j, row in enumerate(reader, start=1):
                bar.update(j)
                try:
                    unixtime = time.mktime(
                        time.strptime(row[0], '%a %d %b %Y %H:%M:%S %Z'))
                except ValueError:
                    # most likely error if unixtime is already in first col
                    #   -> write unmodified
                    writer.writerow(row)
                else:
                    writer.writerow([unixtime] + row)
        os.rename(csvout, csvfile)

def sqlitemerge(db_in = None, db_out = None):
    """Merge two sqlite databases"""
    if not db_out:
        try:
            os.environ['CCT']
        except KeyError:
            print('You must specify the environment variable "CCT".')
            return
        else:
            db_out = os.path.join(os.environ['CCT'], 'data', 'TickerStore.db')

    dbin = records.Database('sqlite:///{}'.format(db_in))
    dbout = records.Database('sqlite:///{}'.format(db_out))

    for i, assetpair in enumerate(ALL_ASSET_PAIRS, start=1):
        print('\nAdding asset pair {}/{} ({})'
              .format(i, len(ALL_ASSET_PAIRS),assetpair))
        rows = dbin.query('SELECT * FROM {}'.format(assetpair))
        print('Trying to determine table length ...')
        length = len(rows.all())
        print(length)
        bar = progressbar.ProgressBar(max_value = length)
        for j, row in enumerate(rows, start=1):
            # if j % 100 == 0:
            #     print('\rEntry {}'.format(j), end='')
            bar.update(j)
            try:
                dbout.query('INSERT INTO {} ('
                            'unixtime, time,'
                            'ask_price, ask_whole_lot_volume, ask_lot_volume,'
                            'bid_price, bid_whole_lot_volume, bid_lot_volume,'
                            'last_trade_price, last_trade_volume,'
                            'volume_today, volume_24hrs,'
                            'weighted_volume_today, weighted_volume_24hrs,'
                            'ntraded_today, ntraded_24hrs,'
                            'low_today, low_24hrs,'
                            'high_today, high_24hrs,'
                            'opening_price'
                            ')'
                            'VALUES('
                            ':unixtime, :timestr,'
                            ':ask_price, :ask_whole_lot_volume, :ask_lot_volume,'
                            ':bid_price, :bid_whole_lot_volume, :bid_lot_volume,'
                            ':last_trade_price, :last_trade_volume,'
                            ':volume_today, :volume_24hrs,'
                            ':weighted_volume_today, :weighted_volume_24hrs,'
                            ':ntraded_today, :ntraded_24hrs,'
                            ':low_today, :low_24hrs,'
                            ':high_today, :high_24hrs,'
                            ':opening_price'
                            ')'.format(assetpair),
                            unixtime              = row.unixtime,
                            timestr               = row.time,
                            ask_price             = row.ask_price,
                            ask_whole_lot_volume  = row.ask_whole_lot_volume,
                            ask_lot_volume        = row.ask_lot_volume,
                            bid_price             = row.bid_price,
                            bid_whole_lot_volume  = row.bid_whole_lot_volume,
                            bid_lot_volume        = row.bid_lot_volume,
                            last_trade_price      = row.last_trade_price,
                            last_trade_volume     = row.last_trade_volume,
                            volume_today          = row.volume_today,
                            volume_24hrs          = row.volume_24hrs,
                            weighted_volume_today = row.weighted_volume_today,
                            weighted_volume_24hrs = row.weighted_volume_24hrs,
                            ntraded_today         = row.ntraded_today,
                            ntraded_24hrs         = row.ntraded_24hrs,
                            low_today             = row.low_today,
                            low_24hrs             = row.low_24hrs,
                            high_today            = row.high_today,
                            high_24hrs            = row.high_24hrs,
                            opening_price         = row.opening_price)
            except sqlalchemy.exc.IntegrityError:
                """
                  entry exists
                """
                pass


def sqlitesplit(db_in):
    """Split a database based on their date information"""
    try:
        os.environ['CCT']
    except KeyError:
        print('You must specify the environment variable "CCT".')
        return

    db_header = 'unixtime NUMERIC PRIMARY KEY, time TEXT, '
    for eachName in names[2:]:
        db_header += '{} NUMERIC, '.format(eachName)
    db_header = db_header.rstrip(', ')
    with records.Database('sqlite:///{}'.format(db_in)) as dbin:
        day = 0
        dbout = None
        for i, assetpair in enumerate(ALL_ASSET_PAIRS, start=1):
            print('\nAdding asset pair {}/{} ({})'
                  .format(i, len(ALL_ASSET_PAIRS),assetpair))
            rows = dbin.query('SELECT * FROM {}'.format(assetpair))
            print('Trying to determine table length ...')
            length = len(rows.all())
            bar = progressbar.ProgressBar(max_value = length)
            for j, row in enumerate(rows, start=1):
                bar.update(j)
                if day != time.gmtime(row.unixtime).tm_mday:
                    t = time.gmtime(row.unixtime)
                    day = t.tm_mday
                    db_out_dir = os.path.join(os.environ['CCT'],
                                  'databases', 'TickerStore',
                                  str(t.tm_year), str(t.tm_mon))
                    try:
                        os.makedirs(db_out_dir)
                    except OSError:
                        # exists
                        pass
                    if dbout:
                        dbout.close()
                    db_out = os.path.join(db_out_dir, '{}.sqlite3'
                                          .format(t.tm_mday))
                    dbout = records.Database('sqlite:///{}'.format(db_out))

                dbout.query('CREATE TABLE IF NOT EXISTS {} ({})'
                         .format(assetpair, db_header))
                try:
                    dbout.query('INSERT INTO {} ('
                                'unixtime, time,'
                                'ask_price, ask_whole_lot_volume, ask_lot_volume,'
                                'bid_price, bid_whole_lot_volume, bid_lot_volume,'
                                'last_trade_price, last_trade_volume,'
                                'volume_today, volume_24hrs,'
                                'weighted_volume_today, weighted_volume_24hrs,'
                                'ntraded_today, ntraded_24hrs,'
                                'low_today, low_24hrs,'
                                'high_today, high_24hrs,'
                                'opening_price'
                                ')'
                                'VALUES('
                                ':unixtime, :timestr,'
                                ':ask_price, :ask_whole_lot_volume, :ask_lot_volume,'
                                ':bid_price, :bid_whole_lot_volume, :bid_lot_volume,'
                                ':last_trade_price, :last_trade_volume,'
                                ':volume_today, :volume_24hrs,'
                                ':weighted_volume_today, :weighted_volume_24hrs,'
                                ':ntraded_today, :ntraded_24hrs,'
                                ':low_today, :low_24hrs,'
                                ':high_today, :high_24hrs,'
                                ':opening_price'
                                ')'.format(assetpair),
                                unixtime              = row.unixtime,
                                timestr               = row.time,
                                ask_price             = row.ask_price,
                                ask_whole_lot_volume  = row.ask_whole_lot_volume,
                                ask_lot_volume        = row.ask_lot_volume,
                                bid_price             = row.bid_price,
                                bid_whole_lot_volume  = row.bid_whole_lot_volume,
                                bid_lot_volume        = row.bid_lot_volume,
                                last_trade_price      = row.last_trade_price,
                                last_trade_volume     = row.last_trade_volume,
                                volume_today          = row.volume_today,
                                volume_24hrs          = row.volume_24hrs,
                                weighted_volume_today = row.weighted_volume_today,
                                weighted_volume_24hrs = row.weighted_volume_24hrs,
                                ntraded_today         = row.ntraded_today,
                                ntraded_24hrs         = row.ntraded_24hrs,
                                low_today             = row.low_today,
                                low_24hrs             = row.low_24hrs,
                                high_today            = row.high_today,
                                high_24hrs            = row.high_24hrs,
                                opening_price         = row.opening_price)
                except sqlalchemy.exc.IntegrityError:
                    """
                      entry exists
                    """
                    pass
